import express from 'express';
import { addEvent, getAllEvents, getByActor} from '../controller/events'

import {validateNonexistentEvent} from '../middleware/event';
import { validateNewEvent}  from '../middleware/validations/event';

const router = express.Router();

router.post('/', validateNewEvent, validateNonexistentEvent, addEvent);
router.get('/', getAllEvents);
router.get('/actors/:actorId', getByActor);

export default router;