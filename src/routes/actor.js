import express from 'express';
import {getAllActors, updateActor} from '../controller/actor'
import {validateUnchangedParams, validateActorExist} from '../middleware/actor'

const router  = express.Router();

//geting endpoint for actors
router.get('/', getAllActors);
router.put('/', validateActorExist, validateUnchangedParams, updateActor);

export default router;