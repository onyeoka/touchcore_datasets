import express from 'express';

const router = express.Router();

import { eraseEvents } from '../controller/events'

router.delete('/', eraseEvents);

export default router;