
module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define('Event', {
    id: {
      primaryKey: true,
      type: DataTypes.NUMBER,
      unique: true,
      allowNull: false,
    },
    
      type: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      created_on: {
        type: DataTypes.DATE,
        allowNull: false,
      }
     }, {
       timestamps: false
 
  });
  Event.associate = function(models) {
    Event.belongsTo(models.Actor, {
      foreignKey: 'actorId',
      target: 'id',
      as: 'actor',
      onDelete: 'CASCADE'
    });

    Event.belongsTo(models.Repo, {
      foreignKey: 'repoId',
      target: 'id',
      as: 'repo',
      onDelete: 'CASCADE'
    })
  };
  return Event;
}