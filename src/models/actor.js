// const { DataTypes } = require("sequelize/types");

module.exports = (sequelize, DataTypes) => {
  const Actor = sequelize.define('Actor', {
    id: {
      primaryKey: true,
      allowNull: false,
      unique: true,
      type: DataTypes.NUMBER,
    },
    login: {
    type: DataTypes.STRING,
    allowNull: false
    },
    avatar_url: {
      type:DataTypes.STRING,
      allowNull: false
    }
  },{
    timestamp: false,
  });
  Actor.associate= function(models) {

  };
  return Actor;
}