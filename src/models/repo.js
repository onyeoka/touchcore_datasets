// const { DataTypes } = require("sequelize/types");

module.exports = (sequelize, DataTypes) => {
  const Repo = sequelize.define('Repo', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.NUMBER,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false
  })
  return Repo
}