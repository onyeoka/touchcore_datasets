import { Pool } from 'pg';
import dotenv from 'dotenv';

dotenv.config();

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
});

pool.on('connect', () => {
  // eslint-disable-next-line no-console
  console.log('connected to the db');
});

const createTables = () => {

  const eventId = `CREATE TABLE IF NOT EXISTS
  
    id SERIAL PRIMARY KEY,
  `;
  pool.query(eventId).catch((err) => {
        // eslint-disable-next-line no-console
        console.log(err);
        pool.end();
  })
  const eventType = `CREATE TABLE IF NOT EXISTS
      eventType  VARCHAR(256)
  `;
  pool.query(eventType).catch((err) => {
        // eslint-disable-next-line no-console
        console.log(err);
        pool.end();
  })
  const Actor = `CREATE TABLE IF NOT EXISTS
  actors (
    id SERIAL PRIMARY KEY,
    loginId VARCHAR(256) NOT NULL,
    avatarUrl VARCHAR (128) NOT NULL,
   )`;
  pool.query(Actor).catch((err) => {
    // eslint-disable-next-line no-console
    console.log(err);
    pool.end();
  });
  const EventRepo = `CREATE TABLE IF NOT EXISTS
    eventRepo (
        id SERIAL PRIMARY KEY,
        actors VARCHAR (355) NOT NULL,
        url VARCHAR(256)
        name VARCHAR(256) NOT NULL,
    )`;
  pool.query(EventRepo)
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log(err);
      pool.end();
    });
    const timeStamp = `CREATE TABLE IF NOT EXISTS
    createdOn TIMESTAMP DEFAULT Now(),
          `;
    pool.query(timeStamp).catch((err) => {
          // eslint-disable-next-line no-console
          console.log(err);
          pool.end();
    })

  const alterEvent = `ALTER TABLE event
      ADD CONSTRAINT fk_event_actors FOREIGN KEY (actors) REFERENCES actors(id) ON DELETE CASCADE`;
  pool.query(alterEvent)
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log(err);
      pool.end();
    });

};

pool.on('remove', () => {
  // eslint-disable-next-line no-console
  console.log('repo removed');
  process.exit(0);
});

module.exports = createTables;

require('make-runnable');
